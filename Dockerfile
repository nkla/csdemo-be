FROM openjdk:8-jdk-alpine
VOLUME /tmp

# Application properties as environment variables
ENV spring.profiles.active prod

# Build args
ARG JAR_FILE

COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]