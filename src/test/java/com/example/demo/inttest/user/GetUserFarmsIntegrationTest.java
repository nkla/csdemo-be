package com.example.demo.inttest.user;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.demo.inttest.BaseIntegrationTest;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;

public class GetUserFarmsIntegrationTest extends BaseIntegrationTest {

  @Test
  public void testGetUserFarms_WhenUserIsNotLoggedIn_ExpectStatusUnauthorized() throws Exception {

    mockMvc.perform( //
        get("/api/v0/users/farms") //
    ) //
        .andExpect(status().isUnauthorized());
  }

  @Test
  @WithUserDetails(value = "admin1customer1")
  public void testGetUserFarms_WhenUserLoggedInIsAdmin_ExpectStatusOkWithAllCustomerFarms() throws Exception {

    mockMvc.perform( //
        get("/api/v0/users/farms") //
    ) //
        .andExpect(status().isOk()) //
        .andExpect(content().contentType(MediaType.APPLICATION_JSON)) //
        .andExpect(jsonPath("$.farms").exists()) //
        .andExpect(jsonPath("$.farms").isArray()) //
        .andExpect(jsonPath("$.farms", hasSize(3)));
  }

  @Test
  @WithUserDetails(value = "user1customer1")
  public void testGetUserFarms_WhenUserLoggedInIsNotAdmin_ExpectStatusOkWithOnlyUserFarms() throws Exception {

    mockMvc.perform( //
        get("/api/v0/users/farms") //
    ) //
        .andExpect(status().isOk()) //
        .andExpect(content().contentType(MediaType.APPLICATION_JSON)) //
        .andExpect(jsonPath("$.farms").exists()) //
        .andExpect(jsonPath("$.farms").isArray()) //
        .andExpect(jsonPath("$.farms", hasSize(2)));
  }

}
