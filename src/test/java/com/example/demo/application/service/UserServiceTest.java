package com.example.demo.application.service;

import static org.mockito.Mockito.when;

import com.example.demo.application.dto.Farm;
import com.example.demo.application.response.GetUserFarmsResponse;
import com.example.demo.domain.model.data.type.UserRole;
import com.example.demo.domain.model.entity.CustomerEntity;
import com.example.demo.domain.model.entity.FarmEntity;
import com.example.demo.domain.model.entity.UserEntity;
import com.example.demo.repository.FarmRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.security.CurrentUserProvider;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

  @Mock
  private CurrentUserProvider currentUserProvider;

  @Mock
  private UserRepository userRepository;

  @Mock
  private FarmRepository farmRepository;

  @InjectMocks
  private UserServiceImpl userService;

  @Test(expected = RuntimeException.class)
  public void testGetUserFarms_WhenUserIsNotLoggedIn_ExpectRuntimeException() {

    // GIVEN
    when(currentUserProvider.getId()).thenReturn(null);

    // WHEN
    userService.getUserFarms();
  }

  @Test
  public void testGetUserFarms_WhenUserLoggedInIsNotAdmin_ExpectUserFarms() {

    // GIVEN
    final Long userId = 1L;
    CustomerEntity customer = new CustomerEntity(1L, "CUSTOMER_NAME");
    UserEntity user = new UserEntity(userId, customer, "USERNAME");
    user.addRole(UserRole.USER);

    FarmEntity farm1 = new FarmEntity(1L, customer, "FARM_1");
    FarmEntity farm2 = new FarmEntity(2L, customer, "FARM_2");

    user.addFarm(farm1);
    user.addFarm(farm2);

    when(currentUserProvider.getId()).thenReturn(userId);
    when(userRepository.findOneWithRolesById(userId)).thenReturn(user);

    // WHEN
    GetUserFarmsResponse response = userService.getUserFarms();

    // THEN
    Assert.assertNotNull(response);
    Assert.assertNotNull(response.getFarms());
    Assert.assertEquals(2, response.getFarms().size());
    Assert.assertTrue(response.getFarms().contains(new Farm(farm1)));
  }

}
