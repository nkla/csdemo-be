package com.example.demo.domain.model.entity;

import com.example.demo.domain.model.data.type.UserRole;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.Table;

@Entity
@Table(name = "USERS")
@NamedEntityGraph(name = "Graph.UserWithRoles", attributeNodes = { //
    @NamedAttributeNode("customer"), //
    @NamedAttributeNode("roles") //
})
public class UserEntity extends BaseEntity {

  private static final long serialVersionUID = 1L;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id")
  private CustomerEntity customer;

  @Column(name = "username", nullable = false, unique = true)
  private String username;

  @Column(name = "password", nullable = false)
  private String password;

  @Enumerated(EnumType.STRING)
  @ElementCollection(fetch = FetchType.LAZY)
  @CollectionTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"))
  @Column(name = "role")
  private Set<UserRole> roles;

  @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
  @JoinTable(name = "user_farms", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "farm_id"))
  private List<FarmEntity> farms;

  public UserEntity() {
  }

  public UserEntity(Long id, CustomerEntity customer, String username) {
    this.id = id;
    this.customer = customer;
    this.username = username;
  }

  @Override
  public String toString() {
    return "UserEntity [customer=" + this.customer + ", username=" + this.username + ", password=*****" + ", roles=" + this.roles + ", farms=" + farms + "]";
  }

  public CustomerEntity getCustomer() {
    return this.customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Set<UserRole> getRoles() {
    if (this.roles == null) {
      this.roles = new HashSet<>();
    }
    return this.roles;
  }

  public void setRoles(Set<UserRole> roles) {
    this.roles = roles;
  }

  public void addRole(UserRole role) {
    if (this.roles == null) {
      this.roles = new HashSet<>();
    }
    this.roles.add(role);
  }

  public void removeRole(UserRole role) {
    if (this.roles != null) {
      this.roles.remove(role);
    }
  }

  public List<FarmEntity> getFarms() {
    if (farms == null) {
      farms = new ArrayList<>();
    }
    return farms;
  }

  public void setFarms(List<FarmEntity> farms) {
    this.farms = farms;
  }

  public void addFarm(FarmEntity farm) {
    if (farms == null) {
      farms = new ArrayList<>();
    }
    farms.add(farm);
    farm.getUsers().add(this);
  }

  public void removeFarm(FarmEntity farm) {
    farms.remove(farm);
    farm.getUsers().remove(this);
  }

  public boolean isAdmin() {
    return this.roles != null && this.roles.contains(UserRole.ADMIN);
  }
}
