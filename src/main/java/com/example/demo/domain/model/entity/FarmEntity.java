package com.example.demo.domain.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FARMS")
public class FarmEntity extends BaseEntity {

  private static final long serialVersionUID = 1L;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id")
  private CustomerEntity customer;

  @Column(name = "name", nullable = false)
  private String name;

  @ManyToMany(mappedBy = "farms", fetch = FetchType.LAZY)
  private List<UserEntity> users;

  public FarmEntity() {
  }

  public FarmEntity(Long id, CustomerEntity customer, String name) {
    this.id = id;
    this.customer = customer;
    this.name = name;
  }

  @Override
  public String toString() {
    return "FarmEntity [customer=" + this.customer + ", name=" + this.name + "]";
  }

  public CustomerEntity getCustomer() {
    return this.customer;
  }

  public void setCustomer(CustomerEntity customer) {
    this.customer = customer;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<UserEntity> getUsers() {
    if (users == null) {
      users = new ArrayList<>();
    }
    return users;
  }

  public void setUsers(List<UserEntity> users) {
    this.users = users;
  }
}
