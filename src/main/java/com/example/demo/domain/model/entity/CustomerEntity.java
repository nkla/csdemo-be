package com.example.demo.domain.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CUSTOMERS")
public class CustomerEntity extends BaseEntity {

  private static final long serialVersionUID = 1L;

  @Column(name = "NAME", nullable = false)
  private String name;

  @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  private List<UserEntity> users;

  @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
  private List<FarmEntity> farms;

  public CustomerEntity() {
  }

  public CustomerEntity(Long id, String name) {
    this.id = id;
    this.name = name;
  }

  public void addUser(UserEntity user) {
    if (users == null) {
      users = new ArrayList<>();
    }
    users.add(user);
    user.setCustomer(this);
  }

  public void removeUser(UserEntity user) {
    users.remove(user);
    user.setCustomer(null);
  }

  public void addFarm(FarmEntity farm) {
    if (farms == null) {
      farms = new ArrayList<>();
    }
    farms.add(farm);
    farm.setCustomer(this);
  }

  public void removeFarm(FarmEntity farm) {
    farms.remove(farm);
    farm.setCustomer(null);
  }

}
