package com.example.demo.security;

import com.example.demo.domain.model.entity.UserEntity;
import com.example.demo.repository.UserRepository;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  private UserRepository userRepository;

  public UserDetailsServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserEntity userEntity = this.userRepository.findOneWithRolesByUsername(username);
    if (userEntity == null) {
      throw new UsernameNotFoundException("User not found for username " + username);
    }
    return new UserDetailsImpl(userEntity);
  }

}
