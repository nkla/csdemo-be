package com.example.demo.security;

import com.example.demo.domain.model.data.type.UserRole;
import com.example.demo.domain.model.entity.UserEntity;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentUserProviderImpl implements CurrentUserProvider {

  private static final Logger log = LoggerFactory.getLogger(CurrentUserProviderImpl.class);

  private UserEntity getCurrentUser() {
    try {
      return ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUserEntity();
    } catch (Exception e) {
      log.error("No valid user found authenticated", e);
      return null;
    }
  }

  @Override
  public Long getId() {
    UserEntity user = getCurrentUser();
    if (user != null) {
      return user.getId();
    }
    return null;
  }

  @Override
  public String getUsername() {
    UserEntity user = getCurrentUser();
    if (user != null) {
      return user.getUsername();
    }
    return null;
  }

  @Override
  public Set<UserRole> getRoles() {
    UserEntity user = getCurrentUser();
    if (user != null) {
      return user.getRoles();
    }
    return null;
  }

}
