package com.example.demo.security;

import com.example.demo.domain.model.data.type.UserRole;

import java.util.Set;

public interface CurrentUserProvider {

  Long getId();

  String getUsername();

  Set<UserRole> getRoles();

}
