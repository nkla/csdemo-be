package com.example.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig {

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(11);
  }

  @Configuration
  @Order(1)
  public static class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http //
          .antMatcher("/api/v0/**") //
          .csrf().disable() //
          .authorizeRequests() //
          .anyRequest().authenticated() //
          .and() //
          .httpBasic() //
          .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
    }
  }

  @Configuration
  public static class DefaultSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
      http //
          .csrf().disable() //
          .headers().frameOptions().sameOrigin() //
          .and() //
          .authorizeRequests() //
          .antMatchers("/h2-console/**").permitAll() //
          .antMatchers("/").permitAll() //
          .antMatchers("/login").permitAll() //
          .anyRequest().authenticated() //
          .and() //
          .formLogin() //
          .loginPage("/login") //
          .successHandler(new CustomAuthenticationSuccessHandler()) //
          .and() //
          .logout() //
          .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK)) //
          .clearAuthentication(true);
    }
  }

}
