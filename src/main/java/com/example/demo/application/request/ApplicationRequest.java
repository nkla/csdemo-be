package com.example.demo.application.request;

public abstract class ApplicationRequest {

  @Override
  public abstract String toString();

}
