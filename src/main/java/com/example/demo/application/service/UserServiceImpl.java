package com.example.demo.application.service;

import com.example.demo.application.dto.Farm;
import com.example.demo.application.response.GetUserFarmsResponse;
import com.example.demo.domain.model.entity.FarmEntity;
import com.example.demo.domain.model.entity.UserEntity;
import com.example.demo.repository.FarmRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.security.CurrentUserProvider;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

  private final CurrentUserProvider currentUserProvider;

  private final UserRepository userRepository;

  private final FarmRepository farmRepository;

  public UserServiceImpl(CurrentUserProvider currentUserProvider, UserRepository userRepository, FarmRepository farmRepository) {
    this.currentUserProvider = currentUserProvider;
    this.userRepository = userRepository;
    this.farmRepository = farmRepository;
  }

  @Override
  @Transactional
  @ApplicationService
  public GetUserFarmsResponse getUserFarms() {

    UserEntity user = userRepository.findOneWithRolesById(currentUserProvider.getId());
    if (user == null) {
      throw new RuntimeException("User needs to be logged in to consume this service!");
    }

    List<FarmEntity> farms = user.getFarms();
    if (user.isAdmin()) {
      farms = farmRepository.findAllByCustomer_Id(user.getCustomer().getId());
    }

    GetUserFarmsResponse response = new GetUserFarmsResponse();
    response.setFarms( //
        farms.stream() //
            .map(farm -> new Farm(farm)) //
            .collect(Collectors.toList()) //
    );

    return response;
  }

}
