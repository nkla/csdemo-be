package com.example.demo.application.service;

import com.example.demo.application.response.GetUserFarmsResponse;

public interface UserService {

  GetUserFarmsResponse getUserFarms();

}
