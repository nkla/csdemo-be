package com.example.demo.application.response;

public abstract class ApplicationResponse {

  @Override
  public abstract String toString();

}
