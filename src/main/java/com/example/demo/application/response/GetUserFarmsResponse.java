package com.example.demo.application.response;

import com.example.demo.application.dto.Farm;

import java.util.List;

public class GetUserFarmsResponse extends ApplicationResponse {

  private List<Farm> farms;

  @Override
  public String toString() {
    return "GetUserFarmsResponse [farms=" + farms + "]";
  }

  public List<Farm> getFarms() {
    return farms;
  }

  public void setFarms(List<Farm> farms) {
    this.farms = farms;
  }

}
