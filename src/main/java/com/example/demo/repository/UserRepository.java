package com.example.demo.repository;

import com.example.demo.domain.model.entity.UserEntity;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

  /**
   * Find one user by ID. This method only loads basic user data without roles. If you need roles, use {@link UserRepository#findOneWithRolesById}.
   * 
   * @param id
   *          user ID
   * @return user without roles
   */
  UserEntity findOneById(@Param(value = "id") Long id);

  /**
   * Find one user by ID, including roles.
   * 
   * @param id
   *          user ID
   * @return user with roles or {@code null} if user not found
   */
  @EntityGraph(value = "Graph.UserWithRoles")
  UserEntity findOneWithRolesById(@Param(value = "id") Long id);

  /**
   * Find one user by username. This method only loads basic user data without roles. If you need roles, use {@link UserRepository#findOneWithRolesByUsername}.
   * 
   * @param id
   *          user ID
   * @return user without roles
   */
  UserEntity findOneByUsername(String username);

  /**
   * Find one user by username, including roles.
   * 
   * @param id
   *          user ID
   * @return user with roles or {@code null} if user not found
   */
  @EntityGraph(value = "Graph.UserWithRoles")
  UserEntity findOneWithRolesByUsername(String username);

}
