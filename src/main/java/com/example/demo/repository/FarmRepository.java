package com.example.demo.repository;

import com.example.demo.domain.model.entity.FarmEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FarmRepository extends JpaRepository<FarmEntity, Long> {

  List<FarmEntity> findAllByCustomer_Id(Long id);

}
