package com.example.demo.logging;

import com.example.demo.application.service.ApplicationService;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Order(1)
@Component
public class LoggingAspect {

  Logger log = LoggerFactory.getLogger(LoggingAspect.class);

  @Around("@annotation(annotationApplicationService) && execution(* *(..))")
  public Object log(ProceedingJoinPoint joinPoint, ApplicationService annotationApplicationService) throws Throwable {
    log.info("Method invoked: [{}]", joinPoint.getSignature());
    if (joinPoint.getArgs().length == 1) {
      log.info("Request received: {}", joinPoint.getArgs()[0]);
    }
    Object response = joinPoint.proceed();
    log.info("Method [{}] successfully executed", joinPoint.getSignature());
    if (response != null) {
      log.info("Returning response: {}", response);
    }
    return response;
  }

}
