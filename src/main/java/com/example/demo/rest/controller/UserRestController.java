package com.example.demo.rest.controller;

import com.example.demo.application.response.GetUserFarmsResponse;
import com.example.demo.application.service.UserService;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v0/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {

  private final UserService userService;

  public UserRestController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping(value = "/farms")
  public Object getUserFarms() {
    GetUserFarmsResponse response = userService.getUserFarms();
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
